///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// File: printTime.c
//
//Description: Print timme in correct formating
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "printtime.h"

void printRefTM(struct tm ref){

   int check = mktime(&ref);
   char buff[80];

   if(check == -1){
      printf("ERROR: Unable to make time using mktime\n");
   } else {
      strftime(buff, sizeof(ref), "%a %b %e %I:%M:%S %p %Z %Y", &ref);
      printf("Reference time: %s\n", buff);
   }
}

void printCount(struct tm tmp){
   printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",tmp.tm_year, tmp.tm_yday, tmp.tm_hour, tmp.tm_min, tmp.tm_sec);
   sleep(1);
}
