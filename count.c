///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// file time.c
//
// Description: This file contains function that creates the reference day,
// calculate the differnece in two dats, what day of the year is a give date.
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "count.h"
#include "ctime.h"
#include "printtime.h"

void cUpOrDown(struct tm ref){
   double diff = 0;

   //current time
   time_t rawtime = time(0);

   //compares reference date to current time to determine if program should count up/down
   diff = difftime(time(&rawtime),mktime(&ref));

//   printf("DEBUG diff time = %d\n", diff);

   if(diff < 0){
   //Reference date is in the future count up   
      countUp(ref);
   }else {
   //Reference date in the paset count downs
      countDwn(ref);
   }

}


//Reference day is in the future count up
void countUp(struct tm ref){

   struct tm count;
   
   //current time
   struct tm now;
   time_t rawtime = time(0);
   now = *localtime(&rawtime);
   
   count = dateDiff(ref, now);

   while(1){
      //keep going until user terminates program
      //for loop just to update count
      for(;count.tm_year >= -127;count.tm_year--){
         for(;count.tm_yday >= 0; count.tm_yday--){
            for(;count.tm_hour >= 0; count.tm_hour--){
               for(;count.tm_min >= 0; count.tm_min--){
                  for(;count.tm_sec >= 0; count.tm_sec--){
                     printCount(count);
                  }count.tm_sec = 59;
               }count.tm_min = 59;
            }count.tm_hour = 23;
          }count.tm_yday = 364;
      }
   }
}

void countDwn(struct tm ref){

   struct tm count;

   //current time
   struct tm now;
   time_t rawtime = time(0);
   now = *localtime(&rawtime);

   count = dateDiff(now,ref);
   
   //Adjust count
   if((now.tm_mon - ref.tm_mon) < 0){
   count.tm_year -= 1;
   count.tm_yday = 365 - count.tm_yday;
   count.tm_hour = 23 - count.tm_hour;
   count.tm_min = 60 - count.tm_min;
   }

   while(1){
      //keep going until user terminates program
      //for loop just to update count
      for(;count.tm_year < 128;count.tm_year++){
         for(;count.tm_yday < 365; count.tm_yday++){
            for(;count.tm_hour < 24; count.tm_hour++){
               for(;count.tm_min < 60; count.tm_min++){
                  for(;count.tm_sec < 60; count.tm_sec++){
                     printCount(count);
                  }count.tm_sec = 0;
               }count.tm_min = 0;
            }count.tm_hour = 0;
          }count.tm_yday = 0;
      }
   }
}

