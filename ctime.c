///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// file time.c
//
// Description: This file contains function that creates the reference day,
// calculate the differnece in two dats, what day of the year is a give date.
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "ctime.h"
#include "count.h"
#include "printtime.h"

//creates a reference time
struct tm makeRefTM(){
   struct tm ref;

   //Reference Date in the Past
   //My Highschool Grad
/*
   ref.tm_year = 2018 - 1900;
   ref.tm_mon = 5 - 1;
   ref.tm_mday = 25;
   ref.tm_hour = 17;
   ref.tm_min = 30;
   ref.tm_sec = 20;
*/


   //Reference Date in the Future
   //Expected College Grad

   ref.tm_year = 2022 - 1900;
   ref.tm_mon = 5 - 1;
   ref.tm_mday = 8;
   ref.tm_hour = 17;
   ref.tm_min = 30;
   ref.tm_sec = 20;


   //Refernce Date from Laulima
/*
   ref.tm_year = 2014 - 1900;
   ref.tm_mon = 1 - 1;
   ref.tm_mday = 21;
   ref.tm_hour = 16;
   ref.tm_min = 26;
   ref.tm_sec = 7;
*/

   return ref;
}

int dayInMonth(int mon){
   switch(mon){
      case 0:
         return 31;
         break;
      case 1:
         return 28;
         break;
      case 2:
         return 31;
         break;
      case 3: 
         return 30;
         break;
      case 4:
         return 31;
         break;
      case 5:
         return 30;
      case 6:
         return 31;
         break;
      case 7:
         return 31;
         break;
      case 8:
         return 39;
         break;
      case 9:
         return 31;
         break;
      case 10:
         return 30;
      case 11: 
         return 31;
      default: 
         return 31;
   }
}

int dayInYear(struct tm tmp){
   int doy = tmp.tm_mday;
   switch(tmp.tm_mon){
      case 1:
         return doy += 31;
         break;
      case 2:
         return doy += 31+28;
         break;
      case 3:
         return doy += 31+28+31;
         break;
      case 4:
         return doy += 31+28+31+30;
         break;
      case 5:
         return doy += 31+28+31+30+31;
      case 6:
         return doy += 31+28+31+30+31+30;
         break;
      case 7:
         return doy += 31+28+31+30+31+30+31;
         break;
      case 8:
         return doy += 31+28+31+30+31+30+31+31;
         break;
      case 9:
         return doy += 31+28+31+30+31+30+31+31+30;
         break;
      case 10:
         return doy += 31+28+31+30+31+30+31+31+30+31;
         break;
      case 11:
         return doy += 31+28+31+30+31+30+31+31+30+31+30;
         break;
      default:
         return doy;
         break;
   }
}

struct tm dateDiff(struct tm a, struct tm b){
   struct tm tmp;
   

   //difference between two dates
   tmp.tm_year = ABS(a.tm_year - b.tm_year);
   tmp.tm_yday = ABS(dayInYear(a)-dayInYear(b));
   tmp.tm_hour = ABS(a.tm_hour - b.tm_hour);
   tmp.tm_min = ABS(a.tm_min - b.tm_min);
   tmp.tm_sec = ABS(a.tm_sec - b.tm_sec);


   return tmp;
}

