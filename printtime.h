///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// File: printTime.c
//
//Description: Print timme in correct formating
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "ctime.h"

void printRefTM(struct tm ref);

void printCount(struct tm tmp);
