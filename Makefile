###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04a - Countdown
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 04a - Countdown - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: countdown

countdown: countdown.c ctime.c count.c printtime.c
	gcc -o countdown countdown.c ctime.c count.c printtime.c

ctime.o: ctime.c ctime.h
	gcc -c ctime.c

count.o: count.c count.h
	gcc -c count.c

printtime.o: printtime.c printtime.h
	gcc -c printtime.c

clean:
	rm -f *.o countdown
