///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// file ctime.h
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ABS(a)	   (((a) < 0) ? -(a) : (a))

struct tm makeRefTM();

int dayInYear(struct tm tmp);

struct tm dateDiff(struct tm a, struct tm b);

