///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time:  Tue Jan 21 04:26:07 PM HST 2014
//
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 57 
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 58 
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 59 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 0 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 1 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 2 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 3 
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "ctime.h"
#include "count.h"
#include "printtime.h"

int main(int argc, char* argv[]) {
   
   struct tm ref;
   

   //Creates and returnss hard coded reference time.
   ref = makeRefTM();
   
   printRefTM(ref); 

   cUpOrDown(ref);

   return 0;


}
