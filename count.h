///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// file count.h
//
// @author Christopher Agcanas <agcanas8@hawaii.edu>
// @date   02 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void cUpOrDown(struct tm ref);

void countUp(struct tm ref);

void countDwn(struct tm ref);
